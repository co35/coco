package com.dingdangmaoup.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.dingdangmaoup.common.security.annotation.EnableCustomConfig;
import com.dingdangmaoup.common.security.annotation.EnableRyFeignClients;
import com.dingdangmaoup.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 代码生成
 * 
 *  
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
public class GenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GenApplication.class, args);

    }
}

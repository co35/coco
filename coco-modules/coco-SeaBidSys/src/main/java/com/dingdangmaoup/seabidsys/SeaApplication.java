package com.dingdangmaoup.seabidsys;

import com.dingdangmaoup.common.security.annotation.EnableCustomConfig;
import com.dingdangmaoup.common.security.annotation.EnableRyFeignClients;
import com.dingdangmaoup.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class SeaApplication {
  public static void main(String[] args) {
    SpringApplication.run(SeaApplication.class,args);
  }
}

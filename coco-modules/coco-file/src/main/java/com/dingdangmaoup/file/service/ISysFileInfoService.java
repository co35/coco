package com.dingdangmaoup.file.service;

import com.dingdangmaoup.file.domain.FileInfo;

/**
 * @author dzhao1
 */
public interface ISysFileInfoService {
   int saveFileInfo(FileInfo fileInfo);

   FileInfo getOne( String identifier);
}

package com.dingdangmaoup.file.service.impl;

import com.dingdangmaoup.file.domain.FileInfo;
import com.dingdangmaoup.file.mapper.SysFileInfoMapper;
import com.dingdangmaoup.file.service.ISysFileInfoService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class ISysFileInfoServiceImpl implements ISysFileInfoService {
@Resource
  SysFileInfoMapper sysFileInfoMapper;
  @Override
  public int saveFileInfo(FileInfo fileInfo) {
    return sysFileInfoMapper.insertFile(fileInfo);
  }

  @Override
  public FileInfo getOne( String identifier) {
    return sysFileInfoMapper.selectOneByFilenameAndIdentifier(identifier);
  }
}

package com.dingdangmaoup.file.mapper;

import com.dingdangmaoup.file.domain.FileInfo;
import com.dingdangmaoup.system.api.domain.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * @author dzhao1
 */
public interface SysFileInfoMapper {


  /**
   * 保存上传文件信息
   * @param fileInfo
   * @return
   */
   int insertFile(FileInfo fileInfo);

  /**
   * 根据md5查询文件
   * @param identifier
   * @return
   */
  FileInfo selectOneByFilenameAndIdentifier(@Param("identifier") String identifier);
}

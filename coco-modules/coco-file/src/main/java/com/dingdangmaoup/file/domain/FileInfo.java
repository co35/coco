package com.dingdangmaoup.file.domain;

import com.dingdangmaoup.common.core.web.domain.BaseEntity;
import java.io.Serializable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 文件表
 * @author dzhao1
 */
public class FileInfo extends BaseEntity implements Serializable {

    /**
     * 文件id
     */
    private Long fileId;
    /**
     * 文件名
     */
    private String filename;
    /**
     * md5
     */
    private String identifier;
    /**
     * 文件大小
     */
    private Long totalSize;
    /**
     * 文件类型
     */
    private String contentType;
    /**
     * 文件地址
     */
    private String location;

    public FileInfo() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("fileId", getFileId())
            .append("filename", getFilename())
            .append("identifier", getIdentifier())
            .append("totalSize", getTotalSize())
            .append("contentType", getContentType())
            .append("location", getLocation())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}

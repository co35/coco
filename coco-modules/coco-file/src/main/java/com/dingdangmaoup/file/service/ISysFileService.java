package com.dingdangmaoup.file.service;

import com.dingdangmaoup.system.api.domain.Chunk;
import com.dingdangmaoup.file.domain.FileInfo;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

/** 文件上传接口 */
public interface ISysFileService {
  /**
   * 文件上传接口
   *
   * @param file 上传的文件
   * @return 访问地址
   * @throws Exception
   */
  String uploadFile(MultipartFile file) throws Exception;

  /**
   * 合并文件块
   *
   * @param fileInfo 合并的文件块信息
   * @return 返回文件地址
   */
  String mergeFile(FileInfo fileInfo) throws IOException;

  /**
   * 分片上传
   * @param chunk
   * @throws IOException
   */
  void uploadChunk(Chunk chunk) throws IOException;

  /**
   * 文件是否已存在=>文件秒传
   * @param fileInfo 文件信息
   * @return 是否存在
   */
  Boolean checkFile(FileInfo fileInfo);
}

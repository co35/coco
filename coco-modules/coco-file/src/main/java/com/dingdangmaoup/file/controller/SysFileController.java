package com.dingdangmaoup.file.controller;

import com.dingdangmaoup.common.redis.service.RedisService;
import com.dingdangmaoup.file.service.ISysFileInfoService;
import com.dingdangmaoup.system.api.domain.Chunk;
import com.dingdangmaoup.file.domain.FileInfo;
import java.io.IOException;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.dingdangmaoup.common.core.domain.R;
import com.dingdangmaoup.common.core.utils.file.FileUtils;
import com.dingdangmaoup.file.service.ISysFileService;
import com.dingdangmaoup.system.api.domain.SysFile;

/** 文件请求处理 */
@RestController
public class SysFileController {
  private static final Logger log = LoggerFactory.getLogger(SysFileController.class);

  @Autowired private ISysFileService sysFileService;
  @Autowired private RedisService redisService;
  @Resource
  ISysFileInfoService sysFileInfoService;
  /**
   * 分片上传
   *
   * @return
   */
  @PostMapping("uploadChunk")
  public R<Boolean> uploadChunk(Chunk chunk) {

    try {
      if (!redisService.hasKey(chunk.getIdentifier() + ":" + chunk.getChunkNumber())){
        sysFileService.uploadChunk(chunk);
      }
      return R.ok(true);
    } catch (IOException e) {
      e.printStackTrace();
      return R.fail();
    }
  }
  /**
   * 检查分片是否存在
   *
   * @return 存在结果
   */
  @GetMapping("checkFile")
  public R<Boolean> checkFile(FileInfo fileInfo) {
   Boolean exist= sysFileService.checkFile(fileInfo);
      return R.ok(exist);
  }

  /**
   * 合并分片
   *
   * @param fileInfo 文件信息
   * @return 文件地址
   */
  @PostMapping("/mergeFile")
  public R<SysFile> mergeFile(@RequestBody FileInfo fileInfo) {
      try {
          String url = sysFileService.mergeFile(fileInfo);
          SysFile sysFile = new SysFile();
          sysFile.setName(FileUtils.getName(url));
          sysFile.setUrl(url);
          return R.ok(sysFile);
      } catch (IOException e) {
          log.error("文件合并失败", e);
          return R.fail(e.getMessage());
      }

  }
  /** 文件上传请求 */
  @PostMapping("upload")
  public R<SysFile> upload(MultipartFile file) {
    try {
      // 上传并返回访问地址
      String url = sysFileService.uploadFile(file);
      SysFile sysFile = new SysFile();
      sysFile.setName(FileUtils.getName(url));
      sysFile.setUrl(url);
      return R.ok(sysFile);
    } catch (Exception e) {
      log.error("上传文件失败", e);
      return R.fail(e.getMessage());
    }
  }
}

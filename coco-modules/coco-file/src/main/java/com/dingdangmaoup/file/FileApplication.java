package com.dingdangmaoup.file;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.dingdangmaoup.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 * 
 *
 */
@EnableCustomSwagger2
@MapperScan("com.dingdangmaoup.**.mapper")
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
@SpringBootApplication
public class FileApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(FileApplication.class, args);
    }
}

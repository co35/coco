package com.dingdangmaoup.file.service;

import com.dingdangmaoup.common.redis.service.RedisService;
import com.dingdangmaoup.common.security.utils.SecurityUtils;
import com.dingdangmaoup.file.utils.FileUtils;
import com.dingdangmaoup.system.api.domain.Chunk;
import com.dingdangmaoup.file.domain.FileInfo;
import com.dingdangmaoup.system.api.model.LoginUser;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.dingdangmaoup.file.utils.FileUploadUtils;

/**
 * 本地文件存储
 * 
 *
 * @author dzhao1
 */
@Primary
@Service
public class LocalSysFileServiceImpl implements ISysFileService
{
    /**
     * 资源映射路径 前缀
     */
    @Value("${file.prefix}")
    public String localFilePrefix;

    /**
     * 域名或本机访问地址
     */
    @Value("${file.domain}")
    public String domain;
    
    /**
     * 上传文件存储在本地的根路径
     */
    @Value("${file.path}")
    private String localFilePath;
    @Resource
    RedisService redisService;
    @Resource
    ISysFileInfoService sysFileInfoService;

    /**
     * 本地文件上传接口
     * 
     * @param file 上传的文件
     * @return 访问地址
     * @throws Exception
     */
    @Override
    public String uploadFile(MultipartFile file) throws Exception
    {
        String name = FileUploadUtils.upload(localFilePath, file);
        String url = domain + localFilePrefix + name;
        return url;
    }

    @Override
    public String mergeFile(FileInfo fileInfo) throws IOException {

        String filename = fileInfo.getFilename();
        String file =localFilePath + File.separator+"oss"+File.separator + fileInfo.getIdentifier() + File.separator + filename;
        String folder = localFilePath + File.separator+"oss"+File.separator + fileInfo.getIdentifier();
        String url = domain + localFilePrefix +"/oss/"+fileInfo.getIdentifier()+"/" + filename;
        FileUtils.merge(file, folder, filename);
        fileInfo.setLocation(url);
        LoginUser loginUser = SecurityUtils.getLoginUser();
        fileInfo.setCreateBy(loginUser.getUsername());
        //保存文件信息
        sysFileInfoService.saveFileInfo(fileInfo);
        return url;
    }

    @Override
    public void uploadChunk(Chunk chunk) throws IOException {
        MultipartFile file = chunk.getFile();
        byte[] bytes = file.getBytes();
        Path path =
            Paths.get(
                FileUtils.generatePath(
                    localFilePath+File.separator+"oss", chunk));
        // 文件写入指定路径
        Files.write(path, bytes);
        redisService.setCacheObject(
            chunk.getIdentifier() + ":" + chunk.getChunkNumber(), chunk, 1L, TimeUnit.DAYS);
    }

    @Override
    public Boolean checkFile(FileInfo fileInfo) {
        FileInfo info  =  sysFileInfoService.getOne(fileInfo.getIdentifier());
        if (info==null){
            return false;
        }
       else if (info.getFilename().equals(fileInfo.getFilename())){
            return true;
        }
       else {
            info.setFilename(fileInfo.getFilename());
            sysFileInfoService.saveFileInfo(info);
           return true;
        }
    }
}

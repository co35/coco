package com.dingdangmaoup.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.dingdangmaoup.common.security.annotation.EnableCustomConfig;
import com.dingdangmaoup.common.security.annotation.EnableRyFeignClients;
import com.dingdangmaoup.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 *  
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class SystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SystemApplication.class, args);

    }
}

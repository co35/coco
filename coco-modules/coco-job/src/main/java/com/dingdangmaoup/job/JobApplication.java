package com.dingdangmaoup.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.dingdangmaoup.common.security.annotation.EnableCustomConfig;
import com.dingdangmaoup.common.security.annotation.EnableRyFeignClients;
import com.dingdangmaoup.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 * 
 *
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
public class JobApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(JobApplication.class, args);

    }
}

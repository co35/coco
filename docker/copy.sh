#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
echo "begin copy sql "
cp ../sql/ry_20210908.sql ./mysql/db
cp ../sql/ry_config_20211118.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../coco-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy coco-gateway "
cp ../coco-gateway/target/coco-gateway.jar ./ruoyi/gateway/jar

echo "begin copy coco-auth "
cp ../coco-auth/target/coco-auth.jar ./ruoyi/auth/jar

echo "begin copy coco-visual "
cp ../coco-visual/coco-monitor/target/coco-visual-monitor.jar  ./ruoyi/visual/monitor/jar

echo "begin copy coco-modules-system "
cp ../coco-modules/coco-system/target/coco-modules-system.jar ./ruoyi/modules/system/jar

echo "begin copy coco-modules-file "
cp ../coco-modules/coco-file/target/coco-modules-file.jar ./ruoyi/modules/file/jar

echo "begin copy coco-modules-job "
cp ../coco-modules/coco-job/target/coco-modules-job.jar ./ruoyi/modules/job/jar

echo "begin copy coco-modules-gen "
cp ../coco-modules/coco-gen/target/coco-modules-gen.jar ./ruoyi/modules/gen/jar


package com.dingdangmaoup.system.api.domain;


import java.beans.Transient;
import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文件块信息
 * @author dzhao1
 */
public class Chunk implements Serializable {
//    private Long id;
    /**
     * 当前文件块，从1开始
     */
    private Integer chunkNumber;
    /**
     * 分块大小
     */
    private Long chunkSize;
    /**
     * 当前分块大小
     */
    private Long currentChunkSize;
    /**
     * 总大小
     */
    private Long totalSize;
    /**
     * 文件标识
     */
    private String identifier;
    /**
     * 文件名
     */
    private String filename;
    /**
     * 相对路径
     */
    private String relativePath;
    /**
     * 总块数
     */
    private Integer totalChunks;
    /**
     * 文件类型
     */
    private String type;
    private transient  MultipartFile file;

    public Chunk() {
    }

    public Chunk(Integer chunkNumber, Long chunkSize, Long currentChunkSize, Long totalSize,
        String identifier, String filename, String relativePath, Integer totalChunks,
        String type, MultipartFile file) {
        this.chunkNumber = chunkNumber;
        this.chunkSize = chunkSize;
        this.currentChunkSize = currentChunkSize;
        this.totalSize = totalSize;
        this.identifier = identifier;
        this.filename = filename;
        this.relativePath = relativePath;
        this.totalChunks = totalChunks;
        this.type = type;
        this.file = file;
    }

    public Integer getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(Integer chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public Long getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(Long chunkSize) {
        this.chunkSize = chunkSize;
    }

    public Long getCurrentChunkSize() {
        return currentChunkSize;
    }

    public void setCurrentChunkSize(Long currentChunkSize) {
        this.currentChunkSize = currentChunkSize;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public Integer getTotalChunks() {
        return totalChunks;
    }

    public void setTotalChunks(Integer totalChunks) {
        this.totalChunks = totalChunks;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Chunk)) {
            return false;
        }

        Chunk chunk = (Chunk) o;

        if (getChunkNumber() != null ? !getChunkNumber().equals(chunk.getChunkNumber())
            : chunk.getChunkNumber() != null) {
            return false;
        }
        if (getChunkSize() != null ? !getChunkSize().equals(chunk.getChunkSize())
            : chunk.getChunkSize() != null) {
            return false;
        }
        if (getCurrentChunkSize() != null ? !getCurrentChunkSize().equals(
            chunk.getCurrentChunkSize())
            : chunk.getCurrentChunkSize() != null) {
            return false;
        }
        if (getTotalSize() != null ? !getTotalSize().equals(chunk.getTotalSize())
            : chunk.getTotalSize() != null) {
            return false;
        }
        if (getIdentifier() != null ? !getIdentifier().equals(chunk.getIdentifier())
            : chunk.getIdentifier() != null) {
            return false;
        }
        if (getFilename() != null ? !getFilename().equals(chunk.getFilename())
            : chunk.getFilename() != null) {
            return false;
        }
        if (getRelativePath() != null ? !getRelativePath().equals(chunk.getRelativePath())
            : chunk.getRelativePath() != null) {
            return false;
        }
        if (getTotalChunks() != null ? !getTotalChunks().equals(chunk.getTotalChunks())
            : chunk.getTotalChunks() != null) {
            return false;
        }
        if (getType() != null ? !getType().equals(chunk.getType()) : chunk.getType() != null) {
            return false;
        }
        return getFile() != null ? getFile().equals(chunk.getFile()) : chunk.getFile() == null;
    }

    @Override
    public int hashCode() {
        int result = getChunkNumber() != null ? getChunkNumber().hashCode() : 0;
        result = 31 * result + (getChunkSize() != null ? getChunkSize().hashCode() : 0);
        result =
            31 * result + (getCurrentChunkSize() != null ? getCurrentChunkSize().hashCode() : 0);
        result = 31 * result + (getTotalSize() != null ? getTotalSize().hashCode() : 0);
        result = 31 * result + (getIdentifier() != null ? getIdentifier().hashCode() : 0);
        result = 31 * result + (getFilename() != null ? getFilename().hashCode() : 0);
        result = 31 * result + (getRelativePath() != null ? getRelativePath().hashCode() : 0);
        result = 31 * result + (getTotalChunks() != null ? getTotalChunks().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getFile() != null ? getFile().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Chunk{" +
            "chunkNumber=" + chunkNumber +
            ", chunkSize=" + chunkSize +
            ", currentChunkSize=" + currentChunkSize +
            ", totalSize=" + totalSize +
            ", identifier='" + identifier + '\'' +
            ", filename='" + filename + '\'' +
            ", relativePath='" + relativePath + '\'' +
            ", totalChunks=" + totalChunks +
            ", type='" + type + '\'' +
            ", file=" + file +
            '}';
    }
}
